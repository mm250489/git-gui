/*
 * Author: Mike McGee
 * 
 * Problem: Create an authentication program
 * 
 * Pseudocode:
 * 1. Initialize variables
 * 2. DOWHILE choice<1
 * 			PROMPT usertypes
 * 				IF usertype = 'Administrator' or usertype = 'Staff' THEN
 * 					WRITE 'Invalid Selection'
 * 				ELSE
 * 					WRITE 'Welcome Student'
 * 					choice = 1
 * 				ENDIF
 * 		ENDDO
 *
 * 3.  DOWHILE countLimit < LIMIT and loopend < 1
 * 			PROMPT uName
 * 			IF uName = correctUname THEN
 * 				DOWHILE countLimit < LIMIT
 * 					IF password = correctPassword THEN
 * 						WRITE 'Welcome ', uName, '!'
 * 						loopend = 1
 * 						break
 * 					ELSE
 * 						WRITE 'Incorrect Password'
 * 						countLimit = countLimit + 1
 * 					ENDDO
 * 			ELSE
 * 				WRITE 'Incorrect Username'
 * 				countLimit = countLimit + 1
 * 			ENDIF
 * 		ENDDO
 * 
 * 4.   IF countLimit >= LIMIT THEN
 * 				WRITE 'Contact System Administrator'
 * 
 */
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Authentication {
	public static final String[] usertypes = { "Administrator", "Student", "Staff" };
	
		public static void main(String[] args) {

				
			final int LIMIT = 3;
			int countLimit = 0;
			int loopend = 0;
			int choice = 0;
			String correctUname = "Mike";
			String correctPassword = "1288";
				
		   while (choice < 1) {
		    JFrame frame = new JFrame("Input");
			   String userChoice = (String) JOptionPane.showInputDialog(frame, 
			       "Choose your user type",
			       "User Selection",
			       JOptionPane.QUESTION_MESSAGE, 
			       null,
			       usertypes, 
			       usertypes[0]);
				
			   switch(userChoice) {
			   case "Student":
			  	 JOptionPane.showMessageDialog(null, "Welcome Student");
			  	 choice = 1;
			  	 break;
			   case "Staff":
			  	 JOptionPane.showMessageDialog(null, "Invalid Selection");
			  	 break;
			   case "Administrator":
			  	 	JOptionPane.showMessageDialog(null, "Invalid Selection");
			  	 	break;
			   }
		   }
			        
			while ((countLimit < LIMIT) && (loopend < 1)) {
				String uName = JOptionPane.showInputDialog("Enter Your Name");
				if(uName.equalsIgnoreCase(correctUname)) {
					while (countLimit < LIMIT) {
						String password = JOptionPane.showInputDialog("Enter Your Password");
						if (password.equals(correctPassword)) {
							JOptionPane.showMessageDialog(null, "Welcome " + uName + "!");	
							loopend = 1;
							break ;
						} else {
							JOptionPane.showMessageDialog(null, "Incorrect Password");
							countLimit = countLimit + 1;
						}
					}	
				} else {
					JOptionPane.showMessageDialog(null, "Incorrect Username");
					countLimit = countLimit + 1;
				}
			}
				
			if(countLimit >= LIMIT){
				JOptionPane.showMessageDialog(null, "Contact System Administrator");
			}
				
	}
		
}
