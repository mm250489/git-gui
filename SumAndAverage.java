/**
* Author: Mike J McGee
*
* Problem: Compute a program that allows the user to input 5 numbers and then display the sum and average of those numbers
*
* Pseudocode:
* 1.  Write prompt message
* 2.  Read number data
* 3.  Determine number sum: number_sum=num1+num2+num3+num4+num5
* 4.  Determine average number: number_avg=number_sum/5.0
* 5.  Create arrays and assign user numbers
* 6.  Display number information
*/
import java.util.Scanner;
import java.io.*;
import java.util.Arrays;

public class SumAndAverage
{
	public static void main(String[] args)
	{
		Scanner keyboard_input = new Scanner(System.in);
		System.out.println("Enter five whole numbers separated by one or more spaces:");
	
		int num1, num2, num3, num4, num5;
		num1 = keyboard_input.nextInt();
		num2 = keyboard_input.nextInt();
		num3 = keyboard_input.nextInt();
		num4 = keyboard_input.nextInt();
		num5 = keyboard_input.nextInt();
		
		int[] UserNumbers;
		UserNumbers = new int[5];
			UserNumbers[0] = num1;
			UserNumbers[1] = num2;
			UserNumbers[2] = num3;
			UserNumbers[3] = num4;
			UserNumbers[4] = num5;
		
		int number_sum = num1 + num2 + num3 + num4 + num5;
		double number_avg = number_sum / 5.0;

		Arrays.sort(UserNumbers);

	
		System.out.println("The median number is: " + UserNumbers[2]);
		System.out.println("The maximum number is: " + UserNumbers[4]);
		System.out.println("The minimum number is: " + UserNumbers[0]);
		System.out.println("The sum of the five numbers is: " + number_sum);
		System.out.println("The average of the five numbers is: " + number_avg);
	
	}
}
