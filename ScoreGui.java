import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class ScoreGui extends JFrame implements ActionListener
{
	
	final int SIZE=7;
	JButton jbtCalculate = new JButton("Calculate Score");

	JTextField []jtxtScore = new JTextField[SIZE];
	JTextField []jtxtWeight = new JTextField[SIZE];
	JTextField jtxtName = new JTextField(20);
	
	
	JLabel [] jlblModuleName = new JLabel[SIZE];
	
	
	JLabel jlblOutput = new JLabel("0");
	JPanel scorePanel = new JPanel();
	JPanel buttonPanel = new JPanel();
	
	double [] scores = new double[SIZE];
	double [] weights = new double[SIZE];
	
	public ScoreGui() {
		
		jlblModuleName[0] = new JLabel("Exam 1/Weight");
		jlblModuleName[1] = new JLabel("Exam 2/Weight");
		jlblModuleName[2] = new JLabel("Exam 3/Weight");
		jlblModuleName[3] = new JLabel("Exam 4/Weight");
		jlblModuleName[4] = new JLabel("Exam 5/Weight");
		jlblModuleName[5] = new JLabel("Exam 6/Weight");
		jlblModuleName[6] = new JLabel("Exam 7/Weight");
		
		scorePanel.setBackground(Color.lightGray);
		buttonPanel.setBackground(Color.blue);
		
		scorePanel.setLayout(new GridLayout(7,3));
		
		
		for (int i=0; i<SIZE; i++) {
			
			scorePanel.add(jlblModuleName[i]);
			jtxtScore[i] = new JTextField(8);
			scorePanel.add(jtxtScore[i]);
			jtxtWeight[i] = new JTextField(8);
			scorePanel.add(jtxtWeight[i]);
			
		}
		
		
		//buttonPanel.add(jlblOutput);
		//buttonPanel.add(jtxtName);
		buttonPanel.add(jbtCalculate);
		jbtCalculate.addActionListener(this);
		
		add(scorePanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
		
		
		setVisible(true);
		pack();
		setLocationRelativeTo(null);
		
	}
	
	public double determineScore(double []score, double []weight) {
		double grade = 0.0;
		
		grade = (score[0] * weight[0]) +
				(score[1] * weight[1]) +
				(score[2] * weight[2]) +
				(score[3] * weight[3]) +
				(score[4] * weight[4]) +
				(score[5] * weight[5]) +
				(score[6] * weight[6]);
				
		return grade;
	}
	
	public char determineLetterGrade(double score) {
		char letter = 'N';
		
		
		return letter;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		//parse the string to double to calculate
		for(int i=0; i<4; i++) {
			scores [i] = Double.parseDouble(jtxtScore[i].getText());
			weights [i] = Double.parseDouble(jtxtWeight[i].getText());
		}
		
		jtxtScore[6].setText(determineScore(scores, weights) + " ");
		
	}
	
	
	

}
