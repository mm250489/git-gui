/**
 * @author Mike McGee
 * Pseudocode:
 * START
 * 1. Read room types
 * 2. Call constructors
 * 3. Set flooring type
 * 4. Set number of windows
 * 5. Set paint color
 * 6. Print 'Flooring type:',flooring,'Number of Windows',numWindows,'Paint Color',wallPaint
 * STOP
 */

import javax.swing.JOptionPane;
public class roomCreator
{
	
	/**
	 * Identify the main argument type
	 * @param args createRoom
	 * Assign values
	 */
	public static void main(String[] args) {
		createRoom firstRoom=new createRoom("Hardwood", 1,"Yellow");
		createRoom secondRoom=new createRoom("Carpeted", 3, "White");
		
		/**
		 * Show Room 1 Dimensions
		 */
		JOptionPane.showMessageDialog(null, "Room 1 Dimensions:" + "\nFloor type: " + firstRoom.getFlooring() + 
				"\nNumber of Windows: " + firstRoom.getWindows() +  
				"\nPaint Color: " + firstRoom.getPaint());
		
		/**
		 * Show Room 2 Dimensions
		 */
		JOptionPane.showMessageDialog(null, "Room 2 Dimensions:" + "\nFloor Type: " + secondRoom.getFlooring() +
				"\nNumber of Windows: " + secondRoom.getWindows() + 
				"\nPaint Color: " + secondRoom.getPaint());
	}

}
