/**
 * createRoom class is used to create a new room object
 * createRoom numberRoom=new createRoom("Flooring", int numWindows, "Paint");
 * numberRoom=get.flooring + get.numWindows + get.wallPaint
 * @author Mike McGee
 *
 */
public class createRoom
{
	String flooring;
	int numWindows;
	String wallPaint;
	String finalLayout;
	
	/**
	 * Initialize room variables to defaults
	 */
	public createRoom() {
		flooring="default";
		numWindows=0;
		wallPaint="black";
		finalLayout="No selections made, please try again";
	}
	
	/**
	 * Initialize room values
	 * @param flooring the type of flooring used
	 * @param numWindows the number of windows in the room
	 * @param wallPaint the paint color of the room
	 */
	public createRoom(String flooring, int numWindows, String wallPaint) {
		this.flooring=flooring;
		this.numWindows=numWindows;
		this.wallPaint=wallPaint;
	}
	
	/**
	 * Set flooring type
	 * @return the current flooring setting
	 */
	public String getFlooring() {
		return this.flooring;
	}
	
	/**
	 * Set the number of windows
	 * @return the current number of windows
	 */
	public int getWindows() {
		return this.numWindows;
	}
	
	/**
	 * Set the paint color
	 * @return the wall paint color
	 */
	public String getPaint() {
		return this.wallPaint;
	}
	
	

}
